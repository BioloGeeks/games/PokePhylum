# PokePhylum

Un phylogénie des Pokemon.

***

## Description

Ce projet consiste en une activité de phylogénie pour des lycéens.
Il s'agit de construire des arbres phylogénétiques à partir de matrice de caractères 

<!-- ## Visuals -->

<!-- ## Installation -->

<!-- ## Usage -->

<!-- ## Support -->

<!-- ## Roadmap -->

<!-- ## Contributing -->

## Licence

Les documents générés dans le cadre de ce projet sont sous licence Creative-Commons Attributions Partage dans les même conditions.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Project status

Ce projet est balbutiant.

## Références

- [Phylogeny-Pokemon.pdf](https://improbable.com/airchives/paperair/volume18/v18i4/Phylogeny-Pokemon.pdf)  
- [Atelier sur le développement des pokémons | JeBiF RSG France](https://jebif.fr/ateliers-de-vulgarisation/atelier-sur-le-developpement-des-pokemons/)
- [jebif poster atelier_pokemon_evolution.pdf](https://jebif.fr/wp-content/uploads/2016/10/atelier_pokemon_evolution.pdf)
- [Matrice de caractères de Pokémons](https://framacalc.org/0L7txJ5ST3)