---
title: "Pokemon Phylogeny with GGDendro"
author: "Samuel Ortion"
format: html
editor: visual
bibliography: ../PokePhylum.bib
---

This idea and most of the code originates from [@sept_30_gggallery_nodate]

## Get the data

```{r}
library(tidyverse)

# Extract and tidy the stats table
Pokemon_df <- read_csv("../data/caracteres.csv")

head(Pokemon_df)

# Hierarchical cluster analysis  
Pokemon_clust <- Pokemon_df[, -c(1,2)] %>% 
  dist(., method = "euclidean") %>%
  hclust(., method = "ward.D2")

library(ggdendro)

# Extract the cluster data
Pokemon_dendro <- Pokemon_clust %>% 
  as.dendrogram() %>%
  dendro_data(., type = "rectangle")

# Extract the terminal branches
Pokemon_dendro_tb <- segment(Pokemon_dendro) %>%
                 filter(yend == 0) %>%
                 left_join(label(Pokemon_dendro)[, c(1, 3)], by = "x")

# Create the dendrogram
Pokemon_dg <- ggplot() +
  geom_segment(data = segment(Pokemon_dendro), 
               aes(x = x, y = y, xend = xend, yend = yend)) +
  geom_segment(data = Pokemon_dendro_tb, 
               aes(x = x, y = y, xend = xend, yend = yend),
               show.legend = F) +
  geom_point(data = label(Pokemon_dendro), 
             aes(x = x, y = y), 
             size = 3, show.legend = F) +
  geom_text(data = label(Pokemon_dendro), 
            aes(x = x, y = y-0.1, label = Pokemon_df$`Nom du pokémon`, hjust = -0.2), 
            size = 3, show.legend = F) + 
  coord_flip(clip = "off") +  # Make the tree horizontal
  scale_y_reverse(expand = c(0.15, 0)) +  # Place the tree root on the left
  theme_void()

Pokemon_dg

# ggsave("../img/Pokemon_dendrogram.png", Pokemon_dg, width = 10, height = 10, bg = "white")
```


Let us try to plot the dendrogram with a subset of the species and subset of characters.
